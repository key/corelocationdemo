CoreLocationDemo
================

This project is example application using **CoreLocation** framework for iPhone.
It's too simple app. Build & run to view parameters below:

* Timestamp
* Latitude
* Longitude
* Altitude
* Vertical Accuracy
* Horizontal Accuracy
* Course
* Speed
* Magnetic Heading
* True Heading
* Heading Accuracy

.. image:: https://bitbucket.org/key/corelocationdemo/raw/master/screenshot.png
