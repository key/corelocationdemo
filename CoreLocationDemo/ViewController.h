//
//  ViewController.h
//  CoreLocationDemo
//
//  Created by Mitsukuni Sato on 11/22/12.
//  Copyright (c) 2012 Mitsukuni Sato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface ViewController : UIViewController <CLLocationManagerDelegate, UIAlertViewDelegate, UIApplicationDelegate>

@end
