//
//  AppDelegate.h
//  CoreLocationDemo
//
//  Created by Mitsukuni Sato on 11/22/12.
//  Copyright (c) 2012 Mitsukuni Sato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
