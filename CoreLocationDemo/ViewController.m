//
//  ViewController.m
//  CoreLocationDemo
//
//  Created by Mitsukuni Sato on 11/22/12.
//  Copyright (c) 2012 Mitsukuni Sato. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "ViewController.h"

@interface ViewController ()

@property (strong, nonatomic) CLLocationManager *clManager;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet UILabel *latitudeLabel;
@property (strong, nonatomic) IBOutlet UILabel *longitudeLabel;
@property (strong, nonatomic) IBOutlet UILabel *altitudeLabel;
@property (strong, nonatomic) IBOutlet UILabel *vAccuracyLabel;
@property (strong, nonatomic) IBOutlet UILabel *hAccuracyLabel;
@property (strong, nonatomic) IBOutlet UILabel *timestampLabel;
@property (strong, nonatomic) IBOutlet UILabel *courseLabel;
@property (strong, nonatomic) IBOutlet UILabel *speedLabel;
@property (strong, nonatomic) IBOutlet UILabel *headingAccuracyLabel;
@property (strong, nonatomic) IBOutlet UILabel *magneticHeadingLabel;
@property (strong, nonatomic) IBOutlet UILabel *trueHeadingLabel;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // initialize location manager
    self.clManager = [[CLLocationManager alloc] init];
    [self.clManager setDelegate:self];
    [self.clManager setDesiredAccuracy:kCLLocationAccuracyBest];
    [self.clManager startUpdatingLocation];
    [self.clManager startUpdatingHeading];
    
    [self.mapView setShowsUserLocation:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillDisappear:(BOOL)animated
{
    // stop CLLocationManager
    [self.clManager stopUpdatingLocation];
    [self.clManager stopUpdatingHeading];
    [self setClManager:nil];

    // stop CLLocationManager of MapView
    // GPS icon showing on status bar if you wouldn't disable `setShowUserLocation'
    [self.mapView setShowsUserLocation:NO];
    [self setMapView:nil];

    [self setLatitudeLabel:nil];
    [self setLongitudeLabel:nil];
    [self setAltitudeLabel:nil];
    [self setVAccuracyLabel:nil];
    [self setHAccuracyLabel:nil];
    [self setTimestampLabel:nil];
    [self setCourseLabel:nil];
    [self setSpeedLabel:nil];
    [self setHeadingAccuracyLabel:nil];
    [self setMagneticHeadingLabel:nil];
    [self setTrueHeadingLabel:nil];
}

#pragma mark - CLLocationManagerDelegate
/**
 * update labels when getting new location(s)
 *
 * NOTE:
 * iOS simulator does not output correct altitude. Always returns 0.
 */
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"Location updated: %@", [locations description]);

    if ([locations count] > 0) {
        CLLocation *loc = [locations objectAtIndex:0];

        // update labels
        [self.altitudeLabel setText:[NSString stringWithFormat:@"%.6f", loc.altitude]];
        [self.latitudeLabel setText:[NSString stringWithFormat:@"%.6f", loc.coordinate.latitude]];
        [self.longitudeLabel setText:[NSString stringWithFormat:@"%.6f", loc.coordinate.longitude]];
        [self.vAccuracyLabel setText:[NSString stringWithFormat:@"%.6f", loc.verticalAccuracy]];
        [self.hAccuracyLabel setText:[NSString stringWithFormat:@"%.6f", loc.horizontalAccuracy]];
        [self.courseLabel setText:[NSString stringWithFormat:@"%.6f", loc.course]];
        [self.speedLabel setText:[NSString stringWithFormat:@"%.6f", loc.speed]];
        [self.timestampLabel setText:[loc.timestamp description] ];

        // set center to my location
        [self.mapView setCenterCoordinate:loc.coordinate];
    }
}

/**
 * update labels when getting new heading
 *
 * WARNING!
 * It's never called when using iOS simulator!
 */
- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    NSLog(@"Heading updated: %@", [newHeading description]);

    [self.headingAccuracyLabel setText:[NSString stringWithFormat:@"%.6f", newHeading.headingAccuracy]];
    [self.trueHeadingLabel setText:[NSString stringWithFormat:@"%.6f", newHeading.trueHeading]];
    [self.magneticHeadingLabel setText:[NSString stringWithFormat:@"%.6f", newHeading.magneticHeading]];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [self.timestampLabel setText:@"Could not get location information"];
    [self.altitudeLabel setText:@""];
    [self.latitudeLabel setText:@""];
    [self.longitudeLabel setText:@""];
    [self.vAccuracyLabel setText:@""];
    [self.hAccuracyLabel setText:@""];
    [self.courseLabel setText:@""];
    [self.speedLabel setText:@""];
    [self.headingAccuracyLabel setText:@""];
    [self.trueHeadingLabel setText:@""];
    [self.magneticHeadingLabel setText:@""];

    UIAlertView *alert = [[UIAlertView alloc]
            initWithTitle:nil
                  message:[error localizedDescription]
                 delegate:self
        cancelButtonTitle:@"Dismiss"
        otherButtonTitles:nil];
    [alert show];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    return;
}

@end
