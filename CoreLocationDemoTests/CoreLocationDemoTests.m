//
//  CoreLocationDemoTests.m
//  CoreLocationDemoTests
//
//  Created by Mitsukuni Sato on 11/22/12.
//  Copyright (c) 2012 Mitsukuni Sato. All rights reserved.
//

#import "CoreLocationDemoTests.h"

@implementation CoreLocationDemoTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in CoreLocationDemoTests");
}

@end
